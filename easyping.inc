<?php
// $Id$

/**
 * @file
 * EasyPing include file for various module functions.
 *
 * @ingroup easyping
 */

 /**
 * Get the sites enabled content types and return them in array.
 */

function easyping_get_content_types() {
  
  // dont start from zero as it causes problems with checkboxes.
  $content_types = array_keys(node_type_get_types());
  array_unshift($content_types, '');
  unset($content_types[0]);
  
  return $content_types;
}

function easyping_ping_services($node) {
  global $base_url;
  $url = $base_url . base_path() . drupal_get_path_alias('node/' . $node->nid);
  
  // this will loop through services in variable taken from admin settings and call ping
  $ping_services = preg_split("/[\s,]+/", variable_get('easyping_ping_services'));
  foreach ($ping_services as $service) {
    _easyping_ping_service($service, $node->title, $url);
  }
}

/**
 * Function to ping a service.
 */

function _easyping_ping_service($service, $name = '', $url = '') {
  $result = xmlrpc($service,
    array(
      'weblogUpdates.ping' => array($name, $url),
    )
  );
  if ($result === FALSE) {
    $error = xmlrpc_error_msg();
    $message = 'Ping Failed: ' . $service . ' - Reason: ' . $error;

    // log to Drupal event log if enabled
    if (variable_get('easyping_log_ping_failed') == TRUE) {
      watchdog('EasyPing', check_plain($message), NULL, WATCHDOG_WARNING);
      
      // display failed pings when administer easyping permission is granted
      if (user_access('administer easyping')) {
        drupal_set_message(check_plain($message),  'warning');
      }
    }
  }
  else {

    $message = 'Ping OK: ' . $url . ' - Service: ' . $service;

    // log to Drupal event log if enabled
    if (variable_get('easyping_log_ping_ok') == TRUE) {
      watchdog('EasyPing', check_plain($message), NULL, WATCHDOG_NOTICE);
    }
    //drupal_set_message(t($message));
  }
}