<?php
// $Id$

/**
 * @file
 * Form builder; Configure the EasyPing settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */

function easyping_settings_form($form, &$form_state) {

  module_load_include('inc', 'easyping');

  $form['easyping_ping_services'] = array(
    '#type' => 'textarea',
    '#title' => t('Ping Services'),
    '#default_value' => variable_get('easyping_ping_services'),
    '#description' => t('Enter ping service URLs one per line.'),
    '#rows' => 15,
  );

  $form['easyping_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content Types'),
    '#default_value' => variable_get('easyping_content_types'),
    '#description' => t('Select what content types will be pinged.'),
    '#options' => easyping_get_content_types(),
    '#multiple' => TRUE,
  );
  
  $form['easyping_ping_new'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ping New Posts'),
    '#default_value' => variable_get('easyping_ping_new', ''),
    '#description' => t('Allows new content of the selected content types above to be pinged on creation'),
  );

  $form['easyping_ping_updated'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ping Updated Posts'),
    '#default_value' => variable_get('easyping_ping_updated', ''),
    '#description' => t('Allows existing content of the selected content types above to be pinged on update'),
  );
  
  $form['easyping_ping_unpublished'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ping Unpublished Posts'),
    '#default_value' => variable_get('easyping_ping_unpublished', ''),
    '#description' => t('Allows New or Updated content to be pinged even when it is unpublished. (Enabling this option is not recommended)'),
  );
  
  // is any form of logging enabled?
  $logging_enabled = FALSE;
  if ((variable_get('easyping_log_ping_failed') == TRUE) || (variable_get('easyping_log_ping_ok') == TRUE)) {
    $logging_enabled = TRUE;
  } 
    
  $form['easyping_enable_logging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Logging'),
    '#default_value' => $logging_enabled,
    '#description' => t('Enables logging of ping results in the Drupal event log.'),
    '#element_validate' => array('_easyping_validate_logging_element'),
  );
  
  $form['easyping_log_ping_failed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log Failed Pings'),
    '#default_value' => variable_get('easyping_log_ping_failed', ''),
    '#description' => t('Enables logging of failed ping attempts in the Drupal event log.'),
    '#states' => array(
      'visible' => array(
        ':input[name="easyping_enable_logging"]' => array('checked' => TRUE),
      ),
      'disabled' => array(
        ':input[name="easyping_enable_logging"]' => array('checked' => FALSE),
      ),
    ),
  );
  
  $form['easyping_log_ping_ok'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log Successfull Pings'),
    '#default_value' => variable_get('easyping_log_ping_ok', ''),
    '#description' => t('Enables logging of Successful ping attempts in the Drupal event log.'),
    '#states' => array(
      'visible' => array(
        ':input[name="easyping_enable_logging"]' => array('checked' => TRUE),
      ),
      'disabled' => array(
        ':input[name="easyping_enable_logging"]' => array('checked' => FALSE),
      ),
    ),
  );
  
  return system_settings_form($form);
}

/**
 * Validate the Logging element.
 * Used to disable all logging if unchecked
 */

function _easyping_validate_logging_element($element, &$form_state) {
  // get value from the element.

  $value = $element['#checked'];

  // is any form of logging enabled?
  if ((variable_get('easyping_log_ping_failed') == TRUE) || (variable_get('easyping_log_ping_ok') == TRUE)) {
    $logging_was_enabled = TRUE;
  } 

  if ($value == FALSE) {
    if ($logging_was_enabled == TRUE) {
      drupal_set_message(t('Logging has been disabled'));
    }
    variable_set('easyping_log_ping_failed', FALSE);
    variable_set('easyping_log_ping_ok', FALSE);
  }
}