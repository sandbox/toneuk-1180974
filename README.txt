
What Is EasyPing

Unlike WordPress, the default installation of Drupal has no way of pinging web sites 
to notify them of new content. This means third party sites like http://pingomatic.com 
do not automatically get notified of any new or updated content on your site. This can 
slow down the indexing of pages in search engines because ping services are used to notify 
them of new content.

This module will provide the ability to add a list of ping services from an administration page. 
This is similar to the functionality of WordPress on the "Writing Settings" page. This will make 
your Drupal site notify the listed services of any new or updated content.

The difference between this and the built in WordPress ping, is with this module you can choose 
what content types to ping and whether nodes should be pinged on creation, update or both. 
Logging can also be enabled so that the results of the ping are stored in the Drupal event log. 
This is handy so that you know when ping services are no longer working and just slowing down the adding or amending of content.

Installing EasyPing:

1. Place the entirety of this directory in sites/all/modules/easyping
2. Navigate to admin/modules and Enable EasyPing.
3. Navigate to admin/people/permissions and assign the Administer EasyPing permission.
   This is required to setup modify the settings of the EasyPing module.

How to configure EasyPing:

1. Navigate to admin/config/services/easyping
2. Enter the list of ping services (See example list below).
3. Select the content types to ping.
4. Select whether to ping on new, updated or both.
5. Select logging options.

Example Ping List:
http://ping.blogs.yandex.ru/RPC2
http://rpc.pingomatic.com 
http://rpc.weblogs.com/RPC2
http://ping.blo.gs
http://www.blogpeople.net/servlet/weblogUpdates
http://ping.myblog.jp
http://blog.goo.ne.jp/XMLRPC

The development of this module was sponsored by ZeusArticles.